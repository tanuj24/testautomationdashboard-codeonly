# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect
from .models import Project, TestPlan, TestCycle, Result, TestCyclesList
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from jira import JIRA

import os
import shutil
import datetime
import xmltodict
import json

# Create your views here.

TEST_CYCLES = [
	'Smoke',
	'Sanity', 
	'Functional', 
	'Performance', 
	'Memory', 
	'Stress', 
	'Load', 
	'Compatibility', 
	'NonFunctional'
]


JIRA_CREDENTIALS = {
	
	'username' : 'HProTestAutomation',
	'password' : 'Dashboard@123'
}


def home(request):
	projects = Project.objects.all()
	return render(request, 'home.html', {'projects' : projects})


def execute(request):
	projects = Project.objects.all()
	return render(request, 'run_automated_tests.html', {'projects' : projects})


def status(request):
	projects = Project.objects.all()
	projects_count = len(projects)

	return render(request, 'status.html', {'projects' : projects, 'projects_count' : projects_count})


def about(request, pk):
	project = Project.objects.get(pk=pk)
	return render(request, 'about.html', {'project' : project})


def projects(request):
	projects = Project.objects.all()
	#admin = User.objects.get(username='trastogi')
	_create_project_dirs(projects)
	return render(request, 'projects.html', {'projects' : projects})


def project_testplans(request, pk):
	project = Project.objects.get(pk=pk)
	testplans = project.testplans.all().order_by('-created_at')
	_create_testplan_dirs(project, testplans)
	return render(request, 'project_testplans.html', {'project' : project, 'testplans' : testplans})


def testplan_testcycles(request, pk, testplan_pk):
	testplan = get_object_or_404(TestPlan, project__pk=pk, pk=testplan_pk)
	testcycles = testplan.testcycles.all()

	_create_testcycles_dirs(testplan, testcycles)

	testcycle_attributes = []
	for testcycle in testcycles:
		latest_report = _get_latest_report(testcycle.pk)

		if not latest_report:
			testcycle_attributes.append([testcycle, testcycle.name, ""])
		else:
			testcycle_attributes.append([testcycle, testcycle.name, latest_report.last_executed])

	return render(request, 'testplan_testcycles.html', {'testplan' : testplan, 'testcycle_attributes' : testcycle_attributes})


def new_testcycle(request, pk, testplan_pk):
	testplan = get_object_or_404(TestPlan, project__pk=pk, pk=testplan_pk)
	added_testcycles = [testcycle.name for testcycle in testplan.testcycles.all()]
	_add_remove_testcycles_to_db()

	testcycleslist = TestCyclesList.objects.all()

	if request.method == 'POST':
		sel_testcycles = request.POST.getlist('mymultiselect')

		for i in range(len(sel_testcycles)):
			sel_testcycle = TestCyclesList.objects.get(id=sel_testcycles[i])
			sel_testcycle_name = sel_testcycle.name

			if sel_testcycle_name not in added_testcycles:
				testcycle = TestCycle.objects.create(
					name=sel_testcycle_name, 
					testplan=testplan, 
				)
		return redirect('testplan_testcycles', pk=pk, testplan_pk=testplan_pk)

	return render(request, 'new_testcycle.html', {'testplan' : testplan, 'added_testcycles' : added_testcycles, 'testcycleslist' : testcycleslist})


def report(request, pk, testplan_pk, testcycle_pk):
	testcycle = get_object_or_404(TestCycle, testplan__pk=testplan_pk, pk=testcycle_pk)
	project = Project.objects.get(pk=pk)

	_fetch_valid_reports_and_update(project, testcycle)

	msg = None
	requested_builds = 5
	reports_list=[]
	reports_list_count = 0
	compared_testplan = None
	available_testplans_list_for_comp = []


	# if request.method == 'POST' and request.FILES['myfile']:
	# 	myfile = request.FILES['myfile']
	# 	report_dir = _handle_uploaded_file(request.FILES['myfile'])

	# 	msg = _enter_report_data_in_db(testcycle.id, report_dir)


	if request.method == 'GET' and '5 builds' in request.GET:
		requested_builds = 5
	elif request.method == 'GET' and '10 builds' in request.GET:
		requested_builds = 10
	elif request.method == 'GET' and '20 builds' in request.GET:
		requested_builds = 20


	for tp in project.testplans.all():
		if tp.pk != testcycle.testplan.pk:
			try:
				tc = tp.testcycles.get(name=testcycle.name)
			except:
				continue

			if not _get_latest_report(tc.pk):
				continue

			available_testplans_list_for_comp.append(tp)


	if request.method == 'GET' and 'comparereleases' in request.GET:
		compared_testplans_id = request.GET.getlist('previous_releases_list')

		for compared_testplan_id in compared_testplans_id:
			compared_testplan = TestPlan.objects.get(pk=compared_testplan_id)
		 	compared_testcycle = compared_testplan.testcycles.get(name=testcycle.name)
			reports_list.append(_get_latest_report(compared_testcycle.pk))

		reports_list.append(_get_latest_report(testcycle.pk))
		reports_list_count = len(reports_list)


	latest_report = _get_latest_report(testcycle.pk)
	all_reports_count = _get_all_reports_count(testcycle.pk)

	all_req_reports = _get_all_req_reports_for_this_testcycle(testcycle.pk, requested_builds)
	all_req_reports_count = len(all_req_reports)

	return render(request, 'report.html', 
		{'testcycle' : testcycle, 'project' : project, 'latest_report' : latest_report, 'all_reports_count' : all_reports_count, 'all_req_reports' : all_req_reports, 'all_req_reports_count' : all_req_reports_count, 'requested_builds' : requested_builds, 'msg' : msg, 'reports_list' : reports_list, 'reports_list_count' : reports_list_count, 'compared_testplan' : compared_testplan, 'available_testplans_list_for_comp' : available_testplans_list_for_comp}
	)


def detailed_report(request, pk, testplan_pk, testcycle_pk):

	PRIORITY_LIST = [
		'Blocker',
		'Critical',
		'Major',
		'Minor',
		'Trivial'
	]

	jira_issues_list = None

	testcycle = get_object_or_404(TestCycle, testplan__pk=testplan_pk, pk=testcycle_pk)
	project = Project.objects.get(pk=pk)
	latest_report = _get_latest_report(testcycle.pk)
	jira_issues = latest_report.jira_issue_ids
	if jira_issues:
		jira_issues_list = str(jira_issues).split("/")

	jira_project_name = None
	available_affected_versions_list = None
	available_components_list = None
	priority_list = PRIORITY_LIST


	jira = _login_to_jira(JIRA_CREDENTIALS['username'], JIRA_CREDENTIALS['password'])

	try:
		jira_project = jira.project(str(project.jira_key))
	except:
		jira_project = None

	if jira_project:
		jira_project_name = jira_project.name
		available_affected_versions_list = [str(version.name) for version in jira_project.versions]
		available_components_list = [str(component.name) for component in jira_project.components]


	return render(request, 'detailed_report.html', {'jira' : jira, 'testcycle' : testcycle, 'latest_report' : latest_report, 'jira_project_name' : jira_project_name, 'priority_list' : priority_list, 'available_affected_versions_list' :available_affected_versions_list, 'available_components_list' : available_components_list, 'jira_issues_list' : jira_issues_list})


def create_jira_issue(request, pk, testplan_pk, testcycle_pk):

	project = Project.objects.get(pk=pk)

	jira = _login_to_jira(JIRA_CREDENTIALS['username'], JIRA_CREDENTIALS['password'])

	if request.method == 'POST':
		summary = request.POST['summary']
		priority = request.POST['priority']
		description = request.POST['description']
		selected_affected_versions_list = request.POST.getlist('affected_versions')
		affected_versions = [{'name' : str(version)} for version in selected_affected_versions_list]
		selected_components_list = request.POST.getlist('components')
		selected_components = [{'name' : str(component) for component in selected_components_list}]

		jira_issue = jira.create_issue(project=str(project.jira_key), summary=summary, description=description, issuetype='Bug', priority={'name' : priority}, versions=affected_versions, components=selected_components)

		jira_id=str(jira_issue.key)
		latest_report = _get_latest_report(testcycle_pk)

		if not latest_report.jira_issue_ids:
			latest_report.jira_issue_ids = jira_id
		else:
			latest_report.jira_issue_ids = latest_report.jira_issue_ids + "/" + jira_id

		latest_report.save()

		#return HttpResponse(json.dumps({'jira_id': jira_id}))

		return redirect('detailed_report', pk=pk, testplan_pk=testplan_pk, testcycle_pk=testcycle_pk)


def _login_to_jira(username, password):
	global jira_login

	try:
		if jira_login:
			pass
	except:
		jira_login=JIRA("https://jira.harman.com/jira", basic_auth=(username, password))

	return jira_login


def _add_remove_testcycles_to_db():
	testcycleslist = TestCyclesList.objects.all()
	available_testcycles_names = [available_testcycle.name for available_testcycle in testcycleslist]
	for i in range(len(TEST_CYCLES)):
		if TEST_CYCLES[i] not in available_testcycles_names:
			TestCyclesList.objects.create(name=TEST_CYCLES[i])

	for i in range(len(available_testcycles_names)):
		if available_testcycles_names[i] not in TEST_CYCLES:
			tc_to_be_deleted = TestCyclesList.objects.get(name=available_testcycles_names[i])
			tc_to_be_deleted.delete()


def _get_latest_report(testcycle_id):

	latest_report=Result.objects.filter(testcycle_id=testcycle_id).order_by('-last_executed')

	if not latest_report:
		latest_report=None
	else:
		latest_report = latest_report[0]

	return latest_report


def _get_all_reports_count(testcycle_id):

	reports=Result.objects.filter(testcycle_id=testcycle_id)
	return len(reports)


def _get_all_req_reports_for_this_testcycle(testcycle_id, requested_builds):
	
	all_reports=Result.objects.filter(testcycle_id=testcycle_id).order_by('-last_executed')
	req_reports = all_reports[:requested_builds][::-1]

	return req_reports


def _handle_uploaded_file(myfile):

	## Validate if the uploaded file is xml file
	uploaded_xml = myfile

	report_dir = os.path.join(settings.XML_ROOT, datetime.datetime.now().strftime('%Y-%m-%dT%H-%M-%S'))
	os.makedirs(report_dir)

	_parse_uploaded_xml(myfile, report_dir)
	_create_json(report_dir)

	return report_dir


def _parse_uploaded_xml(myfile, report_dir):

	with open(report_dir+"\\report.xml", 'w') as destination:
		for chunks in myfile.chunks():
			destination.write(chunks.rstrip())


def _create_json(report_dir):

	with open(report_dir+"\\report.xml") as f:
		xmlstring = f.read()

	jsonstring = json.dumps(xmltodict.parse(xmlstring), indent = 4)

	with open(report_dir+"\\report.json", 'w') as f:
		f.write(jsonstring)


def _enter_report_data_in_db(testcycle_id, report_dir):

	testcycle = TestCycle.objects.get(id=testcycle_id)

	detailed_report_path = ''
	subdirs = os.listdir(report_dir)
	for subdir in subdirs:
		if os.path.isdir(os.path.join(report_dir, subdir)):
			subdir_path = os.path.join(report_dir, subdir)
			#file_path = os.path.join(subdir_path, 'index.htm')
			files = os.listdir(subdir_path)
			for file in files:
				if file.startswith('index'):
					index_file = file
					break
			
			if index_file:		
				file_path = os.path.join(subdir_path, index_file)
				detailed_report_path = 'TestReports' + file_path.split('TestReports')[1]
				break

	with open(report_dir+"\\report.json") as f:
		obj = json.load(f)

	msg = None
	if str(obj["testreport"]["project"]).upper() == testcycle.testplan.project.name.upper():
		if str(obj["testreport"]["testrelease"]).upper() == testcycle.testplan.name.upper():
			if str(obj["testreport"]["testcycle"]).upper() == testcycle.name.upper():
				report = Result.objects.create(
						last_executed = str(obj["testreport"]["report"]["starttime"]),
						testcycle = testcycle,
						total_testcases = str(obj["testreport"]["report"]["scriptscount"]),
						pass_count = str(obj["testreport"]["report"]["passcount"]),
						fail_count = str(obj["testreport"]["report"]["failcount"]),
						norun_count = str(obj["testreport"]["report"]["noruncount"]),
						duration = str(obj["testreport"]["report"]["duration"]),
						detailed_report = detailed_report_path
						)

			else:
				msg = "Test cycle does not match. Please choose the correct Test cycle and upload again."
		else:
			msg = "Test Release doesn not match. Please choose the correct Test Release and upload again."
	else:
		msg = "Project does not match. Please choose the correct Project and upload again."

	return msg


def _create_project_dirs(projects):

	for project in projects:
		_create_dir(settings.TESTREPORTS_TEMP, project.name)
		_create_dir(settings.TESTREPORTS_ROOT, project.name)


def _create_testplan_dirs(project, testplans):

	for testplan in testplans:
		_create_dir(os.path.join(settings.TESTREPORTS_TEMP, project.name), testplan.name)
		_create_dir(os.path.join(settings.TESTREPORTS_ROOT, project.name), testplan.name)


def _create_testcycles_dirs(testplan, testcycles):

	for testcycle in testcycles:
		_create_dir(os.path.join(os.path.join(settings.TESTREPORTS_TEMP, testplan.project.name), testplan.name), testcycle.name)
		_create_dir(os.path.join(os.path.join(settings.TESTREPORTS_ROOT, testplan.project.name), testplan.name), testcycle.name)
	

def _create_dir(parent_dir, new_dir_name):

	if not os.path.exists(parent_dir):
		os.makedirs(parent_dir)

	new_dir = os.path.join(parent_dir, new_dir_name)
	if not os.path.exists(new_dir):
		os.makedirs(new_dir)


def _fetch_valid_reports_and_update(project, testcycle):

	testcycle_dir_temp = os.path.join(os.path.join(os.path.join(settings.TESTREPORTS_TEMP, project.name), testcycle.testplan.name), testcycle.name)
	testcycle_dir = os.path.join(os.path.join(os.path.join(settings.TESTREPORTS_ROOT, project.name), testcycle.testplan.name), testcycle.name)

	reports_foldername = os.listdir(testcycle_dir_temp)

	if reports_foldername:

		reports_dir_temp = [os.path.join(testcycle_dir_temp, report_foldername) for report_foldername in reports_foldername]
		reports_dir_temp.sort(key=lambda x: os.path.getmtime(x))

		for report_dir_temp in reports_dir_temp:
			report_foldername = os.path.basename(report_dir_temp)
			report_dir = os.path.join(testcycle_dir, report_foldername)

			if not os.path.exists(report_dir):
				shutil.copytree(report_dir_temp, report_dir)
				msg = _udpate_report(report_dir, testcycle)

				if not msg:
					shutil.rmtree(report_dir_temp)
				else:
					shutil.rmtree(report_dir)


def _udpate_report(report_dir, testcycle):

	_create_json(report_dir)
	msg = _enter_report_data_in_db(testcycle.id, report_dir)

	return msg