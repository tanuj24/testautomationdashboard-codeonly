# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

from django.contrib.auth.models import User
from django.conf import settings


class Project(models.Model):
	name=models.CharField(max_length=30, unique=True)
	description=models.TextField(max_length=2000, help_text='Max characters limit is 2000')
	jira_key = models.CharField(max_length=15, unique=True)
	scripting_language=models.CharField(max_length=50)
	tools=models.CharField(max_length=50)
	total_testcases_in_project = models.PositiveSmallIntegerField()
	total_automatable_testcases = models.PositiveSmallIntegerField()
	automated_testcases = models.PositiveSmallIntegerField()

	def __str__(self):
		return self.name


class TestPlan(models.Model):
	name=models.CharField(max_length=50)
	description=models.TextField(max_length=500, help_text='Enter a brief description about this release. For Example: You can enter the details about "New Features added" etc.')
	project=models.ForeignKey(Project, related_name='testplans')
	created_at=models.DateTimeField(auto_now_add=True)
	
	def __str__(self):
		return self.name


class TestCycle(models.Model):
	name=models.CharField(max_length=20)
	testplan=models.ForeignKey(TestPlan, related_name='testcycles')

	def __str__(self):
		return self.name


class Result(models.Model):
	updated_at=models.DateTimeField(auto_now_add=True)
	last_executed = models.CharField(max_length=40)
	testcycle=models.ForeignKey(TestCycle, related_name='results')
	total_testcases=models.PositiveSmallIntegerField()
	pass_count=models.PositiveSmallIntegerField()
	fail_count=models.PositiveSmallIntegerField()
	norun_count=models.PositiveSmallIntegerField()
	duration=models.CharField(max_length=8, default="None")
	detailed_report=models.TextField(max_length=200)
	jira_issue_ids = models.TextField(null=True)

class TestCyclesList(models.Model):
	name=models.CharField(max_length=30, unique=True)