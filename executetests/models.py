# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

from django.contrib.auth.models import User
from django.conf import settings


class JenkinsServer(models.Model):
	name=models.CharField(max_length=50, unique=True)
	url=models.CharField(max_length=50, unique=True)
	username = models.CharField(max_length=20, blank=True)
	password=models.CharField(max_length=20, blank=True)


	def __str__(self):
		return self.name
