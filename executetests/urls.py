"""dashboard URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from . import views

from django.conf import settings
from django.conf.urls.static import static



urlpatterns = [

    url(r'^$', views.jenkins_projects, name='jenkins_projects'),
    url(r'^(?P<pk>\d+)/nodes/$', views.nodes, name='nodes'),
    url(r'^(?P<pk>\d+)/nodes/newnode/$', views.new_node, name='new_node'),
    url(r'^(?P<pk>\d+)/nodes/(?P<node_name>.+?[-()\w]+)/$', views.node_config, name='node_config'),
    url(r'^(?P<pk>\d+)/(?P<view_name>.+?[-()\w]+)/jobs/$', views.project_jobs, name='project_jobs'),
    url(r'^(?P<pk>\d+)/(?P<view_name>.+?[-()\w]+)/jobs/(?P<job_name>.+?[-()\w]+)/builds$', views.job_config, name='job_config'),
    url(r'^(?P<pk>\d+)/(?P<view_name>.+?[-()\w]+)/jobs/(?P<job_name>.+?[-()\w]+)/builds/(?P<build_number>\d+)/output/$', views.console_output, name='console_output'),
]