# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django.http import HttpResponse
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect
from reports.models import Project, TestPlan, TestCycle, Result, TestCyclesList
from .models import JenkinsServer
from django.contrib.auth.models import User
from django.conf import settings

import jenkins
import time
import __builtin__
import json
import xmltodict
import xml.etree.ElementTree as ET
import os

# Create your views here.


node_status_lookup = {
			True  : "Offline",
			False : "Online"
		}

def jenkins_projects(request):
	#projects = Project.objects.all()

	servers = JenkinsServer.objects.all()

	available_servers_name = []
	server_attributes = []
	all_available_servers_attributes = []
	selected_view_name = None
	selected_server = None
	nodes = {}

	for server in servers:
		server_url = "http://" + server.url.split("//")[-1] 
		server_instance = jenkins.Jenkins(server_url, server.username, server.password, timeout=2)
		try:
			if server_instance.wait_for_normal_op(2):
				server_views = _get_all_server_views(server_instance)

				server_attributes = [server, server_views]
				available_servers_name.append(server.name)
		except:
			continue

		all_available_servers_attributes.append(server_attributes)


	if request.method == 'GET' and 'projects_list' in request.GET:
		req = request.GET.get('projects_list')

		if not req:
			nodes={}
		else:
			selected_server_id = req[0]
			selected_view_name = req[1:]
			selected_server = JenkinsServer.objects.get(pk=selected_server_id)
			selected_server_url = "http://" + selected_server.url.split("//")[-1]
			selected_server_instance = jenkins.Jenkins(selected_server_url, selected_server.username, selected_server.password, timeout=2)

			try:
				if selected_server_instance.wait_for_normal_op(2):
					nodes = _get_all_server_slaves_with_status(selected_server_instance)
			except:
				pass

	return render(request, 'jenkins_projects.html', {'all_available_servers_attributes' : all_available_servers_attributes, 'servers' : servers, 'available_servers_name' : available_servers_name, 'nodes' : nodes, 'selected_view_name' : selected_view_name, 'selected_server' : selected_server})


def new_node(request, pk):

	server = JenkinsServer.objects.get(pk=pk)

	USAGE_LOOKUP = {
					'Use this node as much as possible' : False,
					'Only build jobs with label expressions matching this node' : True
	}

	LAUNCH_METHOD_LOOKUP = {
							'Launch agent via Java Web Start' : jenkins.LAUNCHER_JNLP,
							'Launch agent via execution of command on the master' : jenkins.LAUNCHER_COMMAND,
							'Launch slave agents on Unix machines via SSH' : jenkins.LAUNCHER_SSH,
							'Let Jenkins control this windows slave as a Windows service' : jenkins.LAUNCHER_WINDOWS_SERVICE
	}

	executors_countlist = [1,2,3,4,5,6,7,8,9,10]
	usages = ["Use this node as much as possible", "Only build jobs with label expressions matching this node"]
	launch_methods = ["Launch agent via Java Web Start", "Launch agent via execution of command on the master", "Launch slave agents on Unix machines via SSH", "Let Jenkins control this windows slave as a Windows service"]

	if request.method == 'POST':
		node_name = request.POST['name']
		description = request.POST['description']
		no_of_executors = __builtin__.int(request.POST['no_of_executors'])
		remote_directory = request.POST['remoterd']
		labels = request.POST['labels']
		usage = str(request.POST['usage_list'])
		launch_method = str(request.POST['launch_method_list'])
		launcher_parameters = {'workDirSettings' : {'disabled' : False, 'internalDir': 'remoting'}}

		
		server_url = "http://" + server.url.split("//")[-1]
		server_instance = jenkins.Jenkins(server_url, server.username, server.password, timeout=2)

		try:
			if server_instance.wait_for_normal_op(2):
				server_instance.create_node(name=node_name, numExecutors=no_of_executors, nodeDescription=description, remoteFS=remote_directory, labels=labels, exclusive=USAGE_LOOKUP[usage], launcher=LAUNCH_METHOD_LOOKUP[launch_method], launcher_params = launcher_parameters)

				return redirect('node_config', pk=pk, node_name=node_name)
		except:
			pass	

	return render(request, 'new_node.html', {'server' : server, 'executors_countlist' : executors_countlist, 'usages' : usages, 'launch_methods' : launch_methods})


def node_config(request, pk, node_name):

	server = JenkinsServer.objects.get(pk=pk)
	server_url = "http://" + server.url.split("//")[-1]
	server_instance = jenkins.Jenkins(server_url, server.username, server.password, timeout=2)

	labels = []

	node_info = server_instance.get_node_info(str(node_name))
	node_config = server_instance.get_node_config(node_name)
	node_status = node_status_lookup[node_info['offline']]
	try:
		description = node_info['description']
		
	except:
		description = None

	try:
		all_labels = node_info['assignedLabels']
		if len(all_labels) > 1:
			for label in all_labels:
				if label['name'] == node_name:
					continue
				labels.append(str(label['name']))
	except:
		pass

	offline_reason = node_info['offlineCauseReason']
	jnlp_agent_status = node_info['jnlpAgent']
	launch_url = "javaws " + server_url + "/computer/" + node_name + "/slave-agent.jnlp"

	if request.method == "POST":
		if 'delete' in request.POST:
			server_instance.delete_node(node_name)

			return redirect('nodes', pk=pk)

	return render(request, 'node_config.html', {'server' : server, 'server_url' : server_url, 'node_name' : node_name, 'node_status' : node_status, 'offline_reason' : offline_reason, 'description' : description, 'labels' : labels, 'jnlp_agent_status' : jnlp_agent_status, 'launch_url' : launch_url})


def nodes(request, pk):

	server = JenkinsServer.objects.get(pk=pk)
	server_url = "http://" + server.url.split("//")[-1]
	server_instance = jenkins.Jenkins(server_url, server.username, server.password, timeout=2)

	nodes_info = []

	try:	
		if server_instance.wait_for_normal_op(timeout=2):
			nodes = _get_all_server_slaves(server_instance)

			for node in nodes:
				node_info = server_instance.get_node_info(node)
				#node_config = server_instance.get_node_config(node_name)
				node_status = node_status_lookup[node_info['offline']]

				if node_status.upper() == "ONLINE":
					response_time = node_info['monitorData']['hudson.node_monitors.ResponseTimeMonitor']['average']
					architecture = node_info['monitorData']['hudson.node_monitors.ArchitectureMonitor']

				elif node_status.upper() == "OFFLINE":
					response_time = ""
					architecture = ""			

				nodes_info.append([node, architecture, node_status, response_time])

	except:
		pass

	return render(request, 'nodes.html', {'nodes_info' : nodes_info, 'server' : server})


def project_jobs(request, pk, view_name):

	server = JenkinsServer.objects.get(pk=pk)
	server_url = "http://" + server.url.split("//")[-1]
	server_instance = jenkins.Jenkins(server_url, server.username, server.password, timeout=2)
	selected_view_name = str(view_name)

	jobs = []
	jobs_info = []

	try:
		if server_instance.wait_for_normal_op(2):
			jobs = _get_all_jobs(server_instance, selected_view_name)
			for job in jobs:
				#job_info = server_instance.get_job_info(job)
				jobs_info.append([job] + _get_build_info(server_instance, job, _get_build_number(server_instance.get_job_info(job), "lastBuild")))
	except:
		pass

	return render(request, 'project_jobs.html', {'server' : server, 'view_name' : view_name, 'jobs' : jobs, 'jobs_info' : jobs_info})


def job_config(request, pk, view_name, job_name):

	legend_lookup = {

		'lastBuild'           : 'Last Build',
		'lastSuccessfulBuild' : 'Last Successful Build',
		'lastStableBuild'     : 'Last Stable Build',
		'lastFailedBuild'     : 'Last Failed Build',
		'lastUnstableBuild'   : 'Last Unstable Build',
		'lastCompletedBuild'  : 'Last Completed Build'
	}

	server = JenkinsServer.objects.get(pk=pk)
	server_url = "http://" + server.url.split("//")[-1]
	server_instance = jenkins.Jenkins(server_url, server.username, server.password, timeout=2)

	queued_builds = []
	running_status = None
	health_report = {}
	all_builds_info = []
	job_statistics = []
	description = None
	job_build_parameters = []

	job_info = server_instance.get_job_info(job_name)
	builds = job_info['builds']
	description = job_info['description']
	is_job_buildable = job_info['buildable']

	if builds:
		statistics_legends = ["lastSuccessfulBuild", "lastStableBuild", "lastFailedBuild", "lastUnstableBuild", "lastCompletedBuild"]
		for legend in statistics_legends:
			job_statistics.append([legend_lookup[legend]]  + _get_build_info(server_instance, job_name, _get_build_number(job_info, legend)))

		all_reports = job_info['healthReport']
		for report in all_reports:
			health_report[report['description']] = report['score']


		running_status = server_instance.get_build_info(job_name, _get_build_number(job_info, "lastBuild"))['building']

		for build in builds[:20]:
			all_builds_info.append(_get_build_info(server_instance, job_name, build['number']))

	next_build_number = job_info['nextBuildNumber']
	all_queued_builds_ids = _get_all_queued_builds_ids_for_job(server_instance, job_name) 

	if all_queued_builds_ids:
		for count, build_id in enumerate(all_queued_builds_ids):
			queued_builds.append([(next_build_number+count), server_instance.get_queue_item(build_id)['why'], build_id])
	queued_builds.reverse()

	try:
		job_parameterized = job_info['actions'][0]['parameterDefinitions']

		if len(job_parameterized) == 1:

			job_build_parameter_type = job_info['actions'][0]['parameterDefinitions'][0]['type']

			if job_build_parameter_type == "NodeParameterDefinition":
				job_build_parameter_name = job_info['actions'][0]['parameterDefinitions'][0]['name']
				job_config_xmlstr = server_instance.get_job_config(job_name)
				xml_root = ET.fromstring(str(job_config_xmlstr))
				job_build_paramter_values = [value.text for value in xml_root.findall('.//parameterDefinitions//allowedSlaves/string')]
				if (not job_build_paramter_values) | ("ALL (no restriction)" in job_build_paramter_values):
					job_build_paramter_values = ['master'] + _get_all_server_slaves(server_instance) 

				job_build_parameters = [job_build_parameter_type, job_build_parameter_name, job_build_paramter_values]
			
			else:
				job_build_url = server_instance.build_job_url(job_name)
				job_build_parameters = [job_build_parameter_type, job_build_url]
		else:
			job_build_url = server_instance.build_job_url(job_name)
			job_build_parameters = [None, job_build_url]

	except:
		pass


	if request.method == 'POST':

		if 'execute' in request.POST:
			try:
				server_instance.build_job(job_name)
			except:
				pass

		elif 'executewithparameters' in request.POST:
			build_parameter_value = request.POST['job_build_parameters_list']
			job_build_parameter_name = str(job_info['actions'][0]['parameterDefinitions'][0]['name'])
			try:
				server_instance.build_job(job_name, {job_build_parameter_name : build_parameter_value})
			except:
				pass

		elif 'cancelqueued' in request.POST:
			build_id = request.POST.get('cancelqueued')
			server_instance.cancel_queue(build_id)

		elif 'cancelrunning' in request.POST:
			server_instance.stop_build(job_name, _get_build_number(job_info, "lastBuild"))

		elif 'enable' in request.POST:
			server_instance.enable_job(job_name)

		return redirect('job_config', pk=pk, view_name=view_name, job_name=job_name)


	return render(request, 'job_config.html', {'server' : server, 'view_name' : view_name, 'job_name' : job_name, 'job_statistics' : job_statistics, 'queued_builds' : queued_builds, 'health_report' : health_report, 'description' : description, 'running_status' : running_status, 'all_builds_info' : all_builds_info, 'is_job_buildable' : is_job_buildable, 'job_build_parameters' : job_build_parameters})



def console_output(request, pk, view_name, job_name, build_number):

	server = JenkinsServer.objects.get(pk=pk)
	server_url = "http://" + server.url.split("//")[-1]
	server_instance = jenkins.Jenkins(server_url, server.username, server.password, timeout=2)	

	output = None
	build_number = int(build_number)

	try:
		if server_instance.wait_for_normal_op(2):
			output = server_instance.get_build_console_output(job_name, build_number)
	except:
		pass

	return render(request, 'console_output.html', {'server' : server, 'view_name' : view_name, 'job_name' : job_name, 'build_number' : build_number, 'output' : output})



def _get_all_server_views(server):

	all_views = []
	views = server.get_views()
	for view in views:
		if view['name'].upper() == 'ALL':
			continue
		all_views.append(view['name'])

	return all_views


def _get_all_jobs(server, view_name):

	all_jobs = []
	jobs = server.get_jobs(view_name=view_name)
	all_jobs = [job['name'] for job in jobs]

	return all_jobs


def _get_all_server_slaves(server):

	all_nodes_names = []
	nodes = server.get_nodes()
	for node in nodes:
		if node['name'].upper() == 'MASTER':
			continue
		all_nodes_names.append(str(node['name']))

	return all_nodes_names


def _get_all_server_slaves_with_status(server):

	all_nodes = {}
	online_nodes = []
	offline_nodes = []
	nodes = server.get_nodes()
	for node in nodes:
		if node['name'].upper() == 'MASTER':
			continue
		all_nodes[node['name']] = node_status_lookup[node['offline']]

	return all_nodes


def _get_build_number(job_info, build_string):

	try:
		build_number = job_info[build_string]['number']
	except:
		build_number = None

	return build_number


def _get_build_info(server, job_name, build_number):

	req_build_info = []

	if build_number:
		build_info = server.get_build_info(job_name, build_number)
		build_epoch_datetime = build_info['timestamp']
		build_datetime = time.strftime('%B %d, %Y, %I:%M:%S %p', time.localtime(build_epoch_datetime/1000.0))
		
		building_status = build_info['building']
		if not building_status:
			build_status = str(build_info['result'])
		else:
			build_status = "RUNNING"

		req_build_info = [build_number, build_datetime, build_status]

	else:
		req_build_info = ["N/A", "N/A", "N/A"]

	return req_build_info


def _get_all_queued_builds_ids_for_job(server, job_name):

	queued_builds_ids = []

	all_queued_builds = server.get_queue_info()

	for queued_build in all_queued_builds:
		if str(queued_build['task']['name']) == job_name:
			queued_builds_ids.append(queued_build['id'])

	queued_builds_ids.sort()

	return queued_builds_ids

