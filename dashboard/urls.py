"""dashboard URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from reports import views

from django.conf import settings
from django.conf.urls.static import static



urlpatterns = [
	# url(r'^$', views.home, name='home'),
 #    url(r'^admin/', admin.site.urls),
 #    url(r'^execute/', views.execute, name='execute'),
 #    url(r'^projects/$', views.projects, name='projects'),
 #    url(r'^(?P<pk>\d+)/about/', views.about, name='about'),
 #    url(r'^status/$', views.status, name='status'),
 #    url(r'^projects/(?P<pk>\d+)/testplans/$', views.project_testplans, name='project_testplans'),
 #    url(r'^projects/(?P<pk>\d+)/testplans/(?P<testplan_pk>\d+)/testcycles/$', views.testplan_testcycles, name='testplan_testcycles'),
	# url(r'^projects/(?P<pk>\d+)/testplans/(?P<testplan_pk>\d+)/newtestcycle/$', views.new_testcycle, name='new_testcycle'),
	# url(r'^projects/(?P<pk>\d+)/testplans/(?P<testplan_pk>\d+)/testcycles/(?P<testcycle_pk>\d+)/report/$', views.report, name='report')

	url(r'^$', views.home, name='home'),
	url(r'^admin/', admin.site.urls),
    url(r'^jenkins_projects/', include('executetests.urls')),
    url(r'^projects/', include('reports.urls')),
    url(r'^status/$', views.status, name='status'),
]
